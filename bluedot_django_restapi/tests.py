from django.test import TestCase

from bluedot_django_restapi.decorators import datetime_fields
from bluedot_django_restapi.response import Response as R

class DecoratorsTests(TestCase):
    '''Tests for decorators'''

    def test_datetime_fields(self):
        '''Check if `datetime_fields` decorator correctly filters malformed dates'''

        @datetime_fields({'a', 'b'})
        def f(provider, request, query):
            self.assertTrue(type(query['a'] == 'aa'))
            self.assertTrue(type(query['b'] == 'aa'))
            return R.Ok();

        good_query = {'a': '2000-06-06 12:12', 'b': '2000-06-06 12:12'}
        bad_query_1 = {'a': '2000-06-06 12:12', 'b': '2000-06-06T12:12'}
        bad_query_2 = {'a': '2000-06-06 12:12', 'b': '2000-06-06 12:12:12'}
        bad_query_3 = {'a': '2000-06-06 12:12', 'b': '2000-06-06 12:12+0000'}

        self.assertTrue(f(None, None, good_query).is_successful())
        self.assertFalse(f(None, None, bad_query_1).is_successful())
        self.assertFalse(f(None, None, bad_query_2).is_successful())
        self.assertFalse(f(None, None, bad_query_3).is_successful())






