from django.conf.urls import url


def _make_addresses(base, provider_path):
    '''Makes collection and object API addresses and prepares reverse name'''

    if type(provider_path) == str:
        provider_path = (provider_path,)

    path = [base]
    for elem in provider_path:
        path.append(elem)
        path.append(f'(?P<{elem}_id>[0-9a-f]+)')

    collection_address = '/'.join(path[:-1]) + '/$'
    object_address = '/'.join(path) + '/$'
    name = '_'.join(provider_path) + '_api'
    return collection_address, object_address, name


def make_url_patterns(base, providers):
    '''Make url patterns for REST API'''

    urlpatterns = []

    for provider_path, providers in providers.items():
        collection_address, object_address, name = _make_addresses(base, provider_path)
        for address, provider in zip((collection_address, object_address), providers):
            if provider:
                urlpatterns.append(url(address, provider.as_view, name=name))

    return urlpatterns

