import json

from django.http import HttpResponse, HttpResponseBadRequest


class HttpResponseCreated(HttpResponse):
    '''HTTP response with status code 201 (missing in django)'''

    status_code = 201


class HttpResponseNoContent(HttpResponse):
    '''HTTP response with status code 204 (missing in django)'''

    status_code = 204


class HttpResponseNotAcceptable(HttpResponse):
    '''HTTP response with status code 406 (missing in django)'''

    status_code = 406


class HttpResponseConflict(HttpResponse):
    '''HTTP response with status code 409 (missing in django)'''

    status_code = 409


class HttpResponsePreconditionFailed(HttpResponse):
    '''HTTP response with status code 414 (missing in django)'''

    status_code = 412


class Message:
    '''Type containing contents to be sent as response and the response status code.'''

    def __init__(self, contents, response_constructor, code):
        self.contents = contents
        self.response_constructor = response_constructor
        self.code = code

    def response(self):
        '''Builds HTTP response.'''

        if self.contents is not None:
            return self.response_constructor(json.dumps(self.contents))

        else:
            return self.response_constructor()

    def is_successful(self):
        return self.code < 100

    def __str__(self):
        return str(self.contents)


class SuccessMessageBuilder:
    def __init__(self, code, response_constructor):
        self.code = code
        self.response_constructor = response_constructor

    def get_contents(self):
        return self.contents

    def __call__(self, contents=None):
        '''Constructs `Message` with given contents and appropriate HTTP response code.'''

        return Message(contents, self.response_constructor, self.code)


class ErrorMessageBuilder:
    def __init__(self, code, response_constructor, generator):
        self.code = code
        self.response_constructor = response_constructor
        self.generator = generator

    def get_contents(self, *args, **kwargs):
        return {
                'error': self.code,
                'message': self.generator(*args, **kwargs)
            }

    def __call__(self, *args, **kwargs):
        '''Constructs `Message` with given contents and appropriate HTTP response code.'''

        return Message(self.get_contents(*args, **kwargs), self.response_constructor, self.code)


class Response:
    @classmethod
    def define_success(cls, code, name, response_constructor):
        '''(Re)defines success response.'''

        setattr(cls, name, SuccessMessageBuilder(code, response_constructor))

    @classmethod
    def define_error(cls, code, name, response_constructor, generator):
        '''(Re)defines error code.'''

        setattr(cls, name, ErrorMessageBuilder(code, response_constructor, generator))


Response.define_success(0, 'Ok', HttpResponse)
Response.define_success(1, 'Created', HttpResponseCreated)
Response.define_success(3, 'Empty', HttpResponseNoContent)

Response.define_error(100, 'MalformedMessage', HttpResponseBadRequest,
        lambda: 'Request content is malformed')

Response.define_error(101, 'UnsupportedFields', HttpResponseBadRequest,
        lambda x: 'Request contains unsupported fields: {}'.format(', '.join(x)))

Response.define_error(102, 'MissingFields', HttpResponseBadRequest,
        lambda x: 'Some required fields are missing: {}'.format(x))

Response.define_error(103, 'NotAcceptableContentType', HttpResponseNotAcceptable,
        lambda x: 'No acceptable content type is supported. ({})'.format(', '.join(x)))

Response.define_error(104, 'NullQuery', HttpResponseBadRequest,
        lambda: 'The query is empty or has no effect')

Response.define_error(105, 'WrongType', HttpResponseBadRequest,
        lambda x, y: 'Field(s) {} should be {}'.format(x, y))

Response.define_error(106, 'WrongValue', HttpResponseBadRequest,
        lambda x, y: 'Field "{}" should be one of: {}'.format(x, ', '.join(y)))

Response.define_error(107, 'Validation', HttpResponseBadRequest,
        lambda x: 'Validation error: {}'.format(x))

