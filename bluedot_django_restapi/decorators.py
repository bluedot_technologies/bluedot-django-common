import collections
import json
from datetime import datetime

from .response import Response as R


def decode_as_json(callback):
    '''
    Decorator decoding request body as JSON. If decode passes the callback will be called with
    request and query as Python dictionary.
    '''

    def wrap(provider, request, **ids):
        # Decode message
        try:
            query = json.loads(request.body)
        except json.decoder.JSONDecodeError:
            return R.MalformedMessage()

        # Call back
        return callback(provider, request, query, **ids)

    return wrap


class decode_from_url:
    '''Decorator for decoding query from url.'''

    def __init__(self, fields, allow_empty=False):
        self.fields = fields
        self.allow_empty = allow_empty

    def __call__(self, callback):
        def wrap(provider, request, **ids):
            query = {}
            for field in self.fields:
                if field in request.GET:
                    query[field] = request.GET.getlist(field)

            if len(query) == 0 and not self.allow_empty:
                return R.NullQuery()

            # Call back
            return callback(provider, request, query, **ids)

        return wrap


class field_count:
    '''Decorator for checking if request contains expected number of fields.'''

    def __init__(self, count):
        self.count = count

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            if len(query) < self.count:
                return R.MissingFields('query should have three fields')

            elif len(query) > self.count:
                return R.UnsupportedFields('query should have three fields')

            else:
                return callback(provider, request, query, **ids)

        return wrap


class supported_fields:
    '''Decorator for checking if request does not contain unsupported fields.'''

    def __init__(self, supported_fields):
        self.supported_fields = supported_fields

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            # Check if there is at least one field
            if len(query) == 0:
                return R.NullQuery()

            # Check if there are some unexpected fields
            unsupported_fields = query.keys() - self.supported_fields
            if len(unsupported_fields) != 0:
                return R.UnsupportedFields(unsupported_fields)

            # Call back
            return callback(provider, request, query, **ids)

        return wrap


class required_fields:
    '''Decorator for checking if request contains required fields.'''

    def __init__(self, required_fields):
        self.required_fields = required_fields

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            # Check if there are some required fields
            missing_fields = self.required_fields - query.keys()
            if len(missing_fields) != 0:
                return R.MissingFields(missing_fields)

            # Call back
            return callback(provider, request, query, **ids)

        return wrap


class exact_fields:
    '''Decorator for checking if request contains exactly the expected fields.'''

    def __init__(self, expected_fields):
        self.expected_fields = expected_fields

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            # Check if there are some unexpected fields
            unexpected_fields = query.keys() - self.expected_fields
            if len(unexpected_fields) != 0:
                return R.UnsupportedFields(unexpected_fields)

            # Check if there are all expected fields
            missing_fields = self.expected_fields - query.keys()
            if len(missing_fields) != 0:
                return R.MissingFields(missing_fields)

            # Call back
            return callback(provider, request, query, **ids)

        return wrap


class not_empty_string_fields:
    '''Decorator for checking if all passed fields are non-empty strings.'''

    def __init__(self, fields):
        self.fields = fields

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            # Check fields
            wrong_fields = set()
            for field in self.fields:
                if not field in query:
                    continue

                elif not isinstance(query[field], str):
                    wrong_fields.add(field)

                elif query[field] == '':
                    wrong_fields.add(field)

            if len(wrong_fields) == 0:
                # Call back
                return callback(provider, request, query, **ids)

            else:
                return R.WrongType(wrong_fields, 'non-empty string')

        return wrap


class string_fields_with_choices:
    '''Decorator for checking if all passed fields are strings from given set of choices.'''

    def __init__(self, fields):
        self.fields = fields

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            # Check fields
            wrong_fields = set()
            for field, choises in self.fields.items():
                if field in query:
                    elements = query[field]
                    if not isinstance(elements, collections.Iterable) or isinstance(elements, str):
                        elements = [elements]
                    for elem in elements:
                        if elem not in choises:
                            wrong_fields.add(field)

            if len(wrong_fields) == 0:
                # Call back
                return callback(provider, request, query, **ids)

            else:
                return R.WrongType(wrong_fields, 'string from defined set')

        return wrap


class integer_fields:
    '''Decorator for checking if all passed fields are integers.'''

    def __init__(self, fields):
        self.fields = fields

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            # Check fields
            wrong_fields = set()
            for field in self.fields:
                if field in query:
                    try:
                        query[field] = int(query[field])
                    except ValueError:
                        wrong_fields.add(field)

            if len(wrong_fields) == 0:
                # Call back
                return callback(provider, request, query, **ids)

            else:
                return R.WrongType(wrong_fields, 'integer')

        return wrap


class boolean_fields:
    '''Decorator for checking if all passed fields are booleans.'''

    def __init__(self, fields):
        self.fields = fields

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            # Check fields
            wrong_fields = set()
            for field in self.fields:
                if field in query:
                    try:
                        query[field] = bool(query[field])
                    except ValueError:
                        wrong_fields.add(field)

            if len(wrong_fields) == 0:
                # Call back
                return callback(provider, request, query, **ids)

            else:
                return R.WrongType(wrong_fields, 'boolean')

        return wrap


class datetime_fields:
    '''Decorator for checking if all passed fields are datetimes.'''

    def __init__(self, fields):
        self.fields = fields

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            # Check fields
            wrong_fields = set()
            for field in self.fields:
                if field in query:
                    try:
                        query[field] = datetime.strptime(query[field] + '+0000', '%Y-%m-%d %H:%M%z')
                    except ValueError:
                        wrong_fields.add(field)

            if len(wrong_fields) == 0:
                # Call back
                return callback(provider, request, query, **ids)

            else:
                return R.WrongType(wrong_fields, 'datetime')

        return wrap


class field_choices:
    '''Decorator for checking if given field has value from given set of choices.'''

    def __init__(self, field_name, choices):
        self.field_name = field_name
        self.choices = choices

    def __call__(self, callback):
        def wrap(provider, request, query, **ids):
            if query[self.field_name] in self.choices:
                return callback(provider, request, query, **ids)

            else:
                return R.WrongValue(self.field_name, self.choices)

        return wrap

