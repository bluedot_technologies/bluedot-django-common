from django.apps import AppConfig


class BluedotDjangoRestapiConfig(AppConfig):
    name = 'bluedot_django_restapi'
