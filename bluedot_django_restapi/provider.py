from django.core.exceptions import ValidationError
from django.http import HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseNotFound


class ApiProvider:
    '''Base class for REST API providers.'''

    METHOD_DECLARATIONS = set()

    def __init__(self):
        '''Initializes the provider.'''

        pass

    @classmethod
    def load(cls, request):
        '''
        Constructs provider.

        If data from database are needed for construction, this method should be overloaded.
        '''

        return cls()

    @classmethod
    def define_methods(cls):
        '''Checks if handlers for declared methods exist and rewrites them into a dictionary.'''

        handlers = {
                'DELETE': ('get_write_access',  'delete'),
                'GET':    ('get_read_access',   'get'),
                'HEAD':   ('get_read_access',   'head'),
                'PATCH':  ('get_write_access',  'patch'),
                'POST':   ('get_create_access', 'post'),
                'PUT':    ('get_write_access',  'put'),
            }

        # Check if at least one handler method is implemented
        if len(cls.METHOD_DECLARATIONS) == 0:
            raise ValidationError('Every subclass of `ApiProvider` should declare ' \
                                  'at least one handler method')

        # Check  existence of all methods
        methods = set()

        for method_name in cls.METHOD_DECLARATIONS:
            access_method_name, content_method_name = handlers[method_name]
            methods.add(access_method_name)
            methods.add(content_method_name)

        for method_name in methods:
            if not hasattr(cls, method_name):
                raise ValidationError('Declared method "{}" does not exists'.format(method_name))

            method = getattr(cls, method_name)
            if not callable(method):
                raise ValidationError('Declared method "{}" is not callable'.format(method_name))

        # Define handlers
        cls._METHODS = {}
        for method_name in cls.METHOD_DECLARATIONS:
            access_method_name, content_method_name = handlers[method_name]
            access_method = getattr(cls, access_method_name)
            content_method = getattr(cls, content_method_name)
            cls._METHODS[method_name] = (access_method, content_method)

    @classmethod
    def as_view(cls, request, **ids):
        '''
        Calls providers method specific to handling given request.

        If this method is not found returns '405 Method Not Allowed' response.
        If data is not found returns '404 Not Found' response.
        '''

        # Check if method is supported
        if request.method not in cls._METHODS:
            return HttpResponseNotAllowed(cls.get_allowed_methods())

        # Load data and call provider
        provider = cls.load(request, **ids)
        if provider:
            return provider.call(request, **ids)
        else:
            return HttpResponseNotFound()


    def call(self, request, **ids):
        '''
        Calls providers method specific to handling given request.

        If this method is not found returns '405 Method Not Allowed' response.
        '''

        # Get handlers
        access_method, content_method = self._METHODS[request.method]

        # Check access and return content
        if access_method(self, request, **ids):
            return content_method(self, request, **ids).response()

        else:
            return HttpResponseForbidden()

    @classmethod
    def get_allowed_methods(cls):
        '''Returns list of names of allowed HTTP methods'''

        return [http_method for http_method, methods in cls._METHODS.items()]

