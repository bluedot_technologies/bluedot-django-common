import json
from urllib.parse import urlencode

from django.contrib.auth import get_user_model
from django.test import TestCase
User = get_user_model()


class BlueDotTestCase(TestCase):
    '''Base class for unit tests'''

    def get(self, url, status_code=200, data=None, **kwargs):
        response = self.client.get(url, data=data, **kwargs)
        self.assertEqual(response.status_code, status_code, response.content)
        return response

    def head(self, url, status_code=200, **kwargs):
        response = self.client.head(url, **kwargs)
        self.assertEqual(response.status_code, status_code, response.content)
        return response

    def post(self, url, status_code=200, data=dict(), **kwargs):
        response = self.client.post(url, data, content_type='application/json', **kwargs)
        self.assertEqual(response.status_code, status_code, response.content)
        return response

    def put(self, url, status_code=200, data=dict(), **kwargs):
        response = self.client.put(url, data, **kwargs)
        self.assertEqual(response.status_code, status_code, response.content)
        return response

    def patch(self, url, status_code=200, data=dict(), **kwargs):
        response = self.client.patch(url, data, **kwargs)
        self.assertEqual(response.status_code, status_code, response.content)
        return response

    def options(self, url, status_code=200, data=dict(), **kwargs):
        response = self.client.options(url, data, **kwargs)
        self.assertEqual(response.status_code, status_code, response.content)
        return response

    def assertErrorData(self, error, response):
        data = json.loads(response.content)
        self.assertEqual(len(data), 2)
        self.assertTrue('error' in data)
        self.assertTrue('message' in data)
        self.assertEqual(data['error'], error.code, data['message'])

    def force_login(self, username='testuser'):
        self.client.force_login(User.objects.get_or_create(username=username)[0])

