import json

from bluedot_django_composed_page.response import Response as R

from .common_testcases import BlueDotTestCase


class ApiTestCase(BlueDotTestCase):
    '''Base test case for all API tests.'''

    def __init__(self, *args, **kwargs):
        super(ApiTestCase, self).__init__(*args, **kwargs)

        self.empty_json = json.dumps({})
        self.malformed_json = 'this is not JSON'

        if self.get_data is not None:
            self.get_json = json.dumps(self.get_data)
        else:
            self.get_json = None

        if self.post_data is not None:
            self.post_json = json.dumps(self.post_data)
        else:
            self.post_json = None

        if self.patch_data is not None:
            self.patch_json = json.dumps(self.patch_data)
        else:
            self.patch_json = None


class ApiQueryTestCase(ApiTestCase):
    '''Common tests for allowed queries.'''

    def test_allowed_queries(self):
        '''Check which HTTP request method types are allowed in resources API'''

        if len(self.allowed_collection_methods) > 0:
            response = self.options(self.collection, 405)
            self.assertEqual(set(response['Allow'].split(', ')), self.allowed_collection_methods)

        if len(self.allowed_object_methods) > 0:
            response = self.options(self.object, 405)
            self.assertEqual(set(response['Allow'].split(', ')), self.allowed_object_methods)

    def test_simple_get_on_collection(self):
        '''Check if GET on collection returns expected code'''

        if len(self.allowed_collection_methods) == 0:
            expected_response_code = 404
        elif 'GET' not in self.allowed_collection_methods:
            expected_response_code = 405
        else:
            expected_response_code = 200

        self.provide_collection_read_access()
        self.get(self.collection, expected_response_code, data=self.get_data)

    def test_simple_get_on_object(self):
        '''Check if GET on object returns expected code'''

        if self.object is not None:
            if len(self.allowed_object_methods) == 0:
                expected_response_code = 404
            elif 'GET' not in self.allowed_object_methods:
                expected_response_code = 405
            else:
                expected_response_code = 200

            self.provide_object_read_access()
            self.get(self.object, expected_response_code)

    def test_simple_post_on_collection(self):
        '''Check if POST on collection returns expected code'''

        if len(self.allowed_collection_methods) == 0:
            expected_response_code = 404
        elif 'POST' not in self.allowed_collection_methods:
            expected_response_code = 405
        else:
            expected_response_code = 201
            self.provide_collection_create_access()

        self.post(self.collection, expected_response_code, data=self.post_json)

    def test_simple_post_on_object(self):
        '''Check if POST on object returns expected code'''

        if self.object is not None:
            if len(self.allowed_object_methods) == 0:
                expected_response_code = 404
            elif 'POST' not in self.allowed_object_methods:
                expected_response_code = 405
            else:
                expected_response_code = 201
                self.provide_object_create_access()

            self.post(self.object, expected_response_code)

    def test_simple_patch_on_object(self):
        '''Check if PATCH on object returns expected code'''

        if self.object is not None:
            if len(self.allowed_object_methods) == 0:
                expected_response_code = 404
            elif 'PATCH' not in self.allowed_object_methods:
                expected_response_code = 405
            else:
                expected_response_code = 200
                self.provide_object_write_access()

            self.patch(self.object, expected_response_code, data=self.patch_json)

