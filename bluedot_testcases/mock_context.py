
class MockContext(object):
    '''This class replaces global function to make some modules testable.'''

    def __init__(self, mock, module_name, from_module_name, function_name):
        self.mock = mock
        self._module_name = module_name
        self._from_module_name = from_module_name
        self._function_name = function_name

    def __enter__(self):
        def fake(*args):
            return self.fake(*args)

        module = __import__(self._module_name, globals(), locals(), [self._from_module_name], 0)
        self._module = getattr(module, self._from_module_name)

        self._old_function = getattr(self._module, self._function_name)
        setattr(self._module, self._function_name, fake)

        return self

    def __exit__(self ,type, value, traceback):
        setattr(self._module, self._function_name, self._old_function)

