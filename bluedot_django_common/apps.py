from django.apps import AppConfig


class BluedotDjangoCommonConfig(AppConfig):
    name = 'bluedot_django_common'
