'use strict';

var bd = new function() {

class Element {
    constructor(element) {
        this.element = element;
    }

    ok() {
        return (this.element != undefined) && (this.element != null);
    }

    get_html() {
        return this.element.innerHTML;
    }

    set_html(html) {
        this.element.innerHTML = html;
    }

    get_attr(attr_name) {
        return this.element.getAttribute(attr_name);
    }

    get_value() {
        return this.element.value;
    }

    set_value(value) {
        this.element.value = value;
    }

    is_checked() {
        return this.element.checked;
    }

    set_checked(checked) {
        this.element.checked = checked;
    }

    has_style(style_name) {
        return this.element.classList.contains(style_name);
    }

    remove_style(style_name) {
        this.element.classList.remove(style_name);
    }

    add_style(style_name) {
        this.element.classList.add(style_name);
    }

    display(display) {
        this.element.style.display = display;
    }

    hide() {
        this.element.style.display = 'none';
    }

    bind(event_name, callback) {
        this.element.addEventListener(event_name, callback);
    }

    get raw() {
        return this.element;
    }
}

this.Element = Element;


class Data {
    constructor(data) {
        this.data = {}
        for (let param of this.PARAM_NAMES) {
            if (data[param] != undefined) {
                this.data[param] = data[param];
            }
        }
    }

    get PARAM_NAMES() {
        return this.constructor.PARAM_NAMES;
    }

    as_json() {
        return this.data;
    }

    empty() {
        let data = {};
        for (let param of this.PARAM_NAMES) {
            data[param] = '';
        }
        return data;
    }
}

this.Data = Data;


class State {
    constructor(view, name) {
        this.view = view;
        this.name = name;
    }

    enter() {}
    fetched() {}
    failed() {}

    // TODO: Obsolete this function
    make_callbacks() {
        let view = this.view;
        let success_code = this.success_code;

        return {
            onprogress: function() {
                view.set_status_text('The request is in progress...');
            },
            onload: function(pe) {
                let response = undefined;
                if (this.response != '') {
                    try {
                        response = JSON.parse(this.response);
                    } catch (e) {
                        view.set_status_text('Failed to parse server response');
                    }
                }
                if (pe.target.status === success_code) {
                    if (response != undefined) {
                        view.state.fetched(response);
                    } else {
                        view.state.failed();
                    }
                } else {
                    console.log('Server responded: ' + this.response);
                    let msg = 'There was a problem with the request (' + pe.target.status + ').';
                    if (response != undefined && response.message != undefined) {
                        msg += ' ' + response.message + '.';
                    } else {
                        msg += ' Failed to parse response.'
                    }
                    view.set_status_text(msg);
                    view.state.failed();
                }
            },
            onerror: function() {
                view.set_status_text('There was a problem with the request.');
                view.state.failed();
            },
            onabort: function() {
                view.set_status_text('The request was stoped.');
                view.state.failed();
            }
        }
    }

    make_callbacks2() {
        let state = this;

        return {
            onprogress: function() {},
            onload: function(pe) {
                let response = undefined;
                if (this.response != '') {
                    try {
                        response = JSON.parse(this.response);
                    } catch (e) {
                        console.log('Failed to parse server response');
                    }
                }
                if (pe.target.status === state.success_code) {
                    if (response != undefined) {
                        state.fetched(response);
                    } else {
                        state.failed(response);
                    }
                } else {
                    console.log('Server responded: ' + this.response);
                    let msg = 'There was a problem with the request (' + pe.target.status + ').';
                    if (response != undefined && response.message != undefined) {
                        msg += ' ' + response.message + '.';
                    } else {
                        msg += ' Failed to parse response.'
                    }
                    console.log(msg);
                    state.failed(response);
                }
            },
            onerror: function() {
                console.log('There was a problem with the request.');
                state.failed();
            },
            onabort: function() {
                console.log('The request was stoped.');
                state.failed();
            }
        }
    }

}

this.State = State;


this.get_cookie = function(name) {
    if (document.cookie.length == 0) {
        return '';
    }

    let start = document.cookie.indexOf(name + "=");
    if (start == -1) {
        return '';
    }

    start += name.length + 1;
    let end = document.cookie.indexOf(";", start);
    if (end == -1) {
        end = document.cookie.length;
    }

    return unescape(document.cookie.substring(start, end));
}

this.e = function(id, warn_null=true) {
    let element = document.getElementById(id);
    if (warn_null && element == null) {
        console.warn('Element "' + id + '" is null')
    }
    return new Element(element);
}

this.h = function(string) {
    let unescaped = /[&<>"']/g;
    if (string && unescaped.test(string)) {
        let replacements = {
            '"': '&#34;',
            '&': '&#38;',
            "'": '&#39;',
            '<': '&#60;',
            '>': '&#62;',
        };
        string = string.replace(unescaped, function(elem) { return replacements[elem] });
    }
    return string;
}

this.q = function(method, address, callbacks, data) {
    let req = new XMLHttpRequest();
    req.onprogress = callbacks.onprogress;
    req.onload = callbacks.onload;
    req.onerror = callbacks.onerror;
    req.onabort = callbacks.onabort;
    req.open(method, address, true);

    req.setRequestHeader('X-CSRFToken', this.get_cookie('csrftoken'));

    req.setRequestHeader("Accept", "application/json");
    if (data == undefined) {
        req.send();
    } else {
        req.setRequestHeader("Content-Type", "application/json");
        req.send(JSON.stringify(data));
    }
}

}

