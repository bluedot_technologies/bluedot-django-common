'use strict';

function toggle_sidebar() {
    let sidebar = document.getElementById('sidebar');
    console.log('Sidebar width:', sidebar.style.width);
    let width = sidebar.style.width;
    if (width == '0' || width == '0px' || width == '') {
        sidebar.style.width = '75%';
    } else {
        sidebar.style.width = '0';
    }
}

function toggle_content(element_id, style_name) {
    let content = document.getElementById(element_id);
    if (content.classList.contains(style_name)) {
        content.classList.remove(style_name);
    } else {
        content.classList.add(style_name);
        if (content.getBoundingClientRect().top < 0) {
            content.scrollIntoView({'behavior': 'smooth'});
        }
    }
}


