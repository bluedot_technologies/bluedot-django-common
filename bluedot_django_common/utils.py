import pytz
import uuid
from docutils.core import publish_parts


def uuid2str(value):
    '''
    Converts UUID to strings (replacing hyphens).

    Raises `TypeError` if given object is neither `uuid.UUID` not `str`.
    '''

    if isinstance(value, uuid.UUID) or isinstance(value, str):
        result = str(value)
        result = result.replace('-', '')
        return result

    elif value is None:
        return None

    else:
        raise TypeError('Object given to `uuid2str` should be `uuid.UUID` or `str` but is {}' \
                .format(type(value)))


def rst2html(rst):
    '''Converts restructured text to HTML'''

    settings = { 'id_prefix': 'rst-' }
    return publish_parts(rst, writer_name='html', settings_overrides=settings)['html_body']


def datetime2utcstr(datetime):
    '''Converts datetime to simple string'''

    return datetime.astimezone(pytz.utc).strftime('%Y-%m-%d %H:%M')

