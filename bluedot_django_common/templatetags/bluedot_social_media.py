from django.template import Library
from django.utils.safestring import mark_safe

register = Library()

@register.simple_tag
def open_graph_headers(title, description, url, image=None):
    '''Places OpenGraph headers.'''

    h  = f'<meta name="og:title" content="{title}">'
    h += f'<meta name="og:description" content="{description}">'
    h += f'<meta property="og:url" content="{url}">'
    if image:
        h += f'<meta property="og:image" content="{image}">'
    return mark_safe(h)

@register.simple_tag
def facebook_init_script():
    '''Places Facebook code for initializing the API.'''

    return mark_safe('''
        <div id="fb-root"></div>
        <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
        ''')

@register.simple_tag
def facebook_button(href, size='small', show_faces='false', share='true'):
    '''Places Facebook 'like' button.'''

    return mark_safe('<div class="fb-like"' \
                         ' data-href="{href}"' \
                         ' data-layout="button"' \
                         ' data-action="like"' \
                         ' data-size="{size}"' \
                         ' data-show-faces="{show_faces}"' \
                         ' data-share="{share}">' \
                      '</div>' \
                      .format(href=href, size=size, show_faces=show_faces, share=share))

