import json

import user_agents
from django.template import Library

register = Library()


@register.filter(name='as_json')
def as_json(dictionary):
    return json.dumps(dictionary)

@register.filter(name='is_mobile')
def is_mobile(request):
    '''Check if user agent is mobile or not.'''

    user_agent_string = request.META.get('HTTP_USER_AGENT', '')
    user_agent = user_agents.parse(user_agent_string)
    return user_agent.is_mobile

