from django.shortcuts import render
from django.utils.datastructures import MultiValueDictKeyError

from . import payments_paypal

_STATUS_OK = 'ok'
_STATUS_CANCEL = 'cancel'
_STATUS_BAD_CALLBACK = 'bad_callback'
_STATUS_ERROR = 'error'

_PAYPAL = 'PayPal'


def _make_response(request, status, payment_system):
    context = { 'status': status, 'payment_system': payment_system  }
    return render(request, 'bluedot_django_payments/info.html', context)


def paypal_return(request):
    '''Handles PayPal return request.'''

    try:
        payment_id = request.GET['paymentId']
        token = request.GET['token']
        payer_id = request.GET['PayerID']
    except MultiValueDictKeyError:
        return _make_response(request, _STATUS_BAD_CALLBACK, _PAYPAL)

    payment = payments_paypal.Payment.find(payment_id)
    if not payment:
        return _make_response(request, _STATUS_ERROR, _PAYPAL)

    if payment.execute(payer_id):
        return _make_response(request, _STATUS_OK, _PAYPAL)
    else:
        return _make_response(request, _STATUS_ERROR, _PAYPAL)


def paypal_cancel(request):
    '''Handles PayPal cancel request.'''

    try:
        token = request.GET['token']
    except MultiValueDictKeyError:
        return _make_response(request, _STATUS_BAD_CALLBACK, _PAYPAL)

    return _make_response(request, _STATUS_CANCEL, _PAYPAL)

