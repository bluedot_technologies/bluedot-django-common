from django.apps import AppConfig
from django.conf import settings


class BluedotDjangoPaymentsConfig(AppConfig):
    name = 'bluedot_django_payments'

    def ready(self):
        # Configure payments
        from . import config as payments_config
        payments_config.configure(
                uri_base=settings.BLUEDOT_URI_BASE,
                mode=payments_config.MODE_SANDBOX if settings.DEBUG else payments_config.MODE_LIVE,
                paypal_client_id=settings.BLUEDOT_PAYMENTS_PAYPAL['client_id'],
                paypal_client_secret=settings.BLUEDOT_PAYMENTS_PAYPAL['client_secret'],
            )



