from bluedot_testcases import common_testcases, mock_context

from . import models, payments_paypal


class PaypalMockContext(mock_context.MockContext):
    '''Mock context for `paypalrestsdk.Payment`.'''

    class DummyLink:
        def __init__(self, href, rel, method):
            self.href = href
            self.rel = rel
            self.method = method

    class PaypalMock:
        '''Mock for `paypalrestsdk.Payment`.'''

        links = list()
        params = None
        id = None

        def __init__(self, params):
            self.__class__.params = params

        @classmethod
        def find(cls, payment_id):
            payment = cls(dict())
            payment.id = payment_id
            return payment

        def create(self):
            return True

        def execute(self, payer_id):
            return True

    def __init__(self):
        super(PaypalMockContext, self).__init__(
            self.PaypalMock,
            'bluedot_django_payments',
            'payments_paypal',
            'payment_class',
        )

    def fake(self):
        return self.mock

    def set_id(self, id):
        self.mock.id = id

    def add_link(self, href, rel, method):
        self.mock.links.append(self.DummyLink(href, rel, method))


class TestPayments(common_testcases.BlueDotTestCase):
    '''Test payments'''

    def test_creating_payment(self):
        '''Check if correct redirect urls are sent to and approval url is stored in database'''

        id = 'PAY-90H30361WJ130342DLLMPKKQ'
        approval_url = 'DEF'
        self.assertEqual(models.Payment.objects.all().count(), 0)

        with PaypalMockContext() as ctx:
            ctx.set_id(id)
            ctx.add_link('ABC', 'self', 'GET')
            ctx.add_link('DEF', 'approval_url', 'REDIRECT')
            ctx.add_link('GHI', 'execute', 'POST')

            payment = payments_paypal.Payment.construct(5, 'EUR', 'a@example.com', 'item', 'desc')
            payment.create()

            self.assertTrue(ctx.mock.params['redirect_urls']['return_url'].startswith('http'))
            self.assertTrue(ctx.mock.params['redirect_urls']['cancel_url'].startswith('http'))

        payment = models.Payment.objects.get(id=id)
        self.assertEqual(payment.approval_url, 'DEF')


class TestRequests(common_testcases.BlueDotTestCase):
    '''Tests requests'''

    return_url = '/payments/paypal/return?paymentId=PAY-90H30361WJ130342DLLMPKKQ&token=EC-0D353258H4193581B&PayerID=EY3S5PB59BK3S'
    bad_return_url = '/payments/paypal/return?paymentId=PAY-90H30361WJ130342DLLMPKKQ&token=EC-0D353258H4193581B&PayerID2=EY3S5PB59BK3S'
    cancel_url = '/payments/paypal/cancel?paymentId=PAY-90H30361WJ130342DLLMPKKQ'
    bad_cancel_url = '/payments/paypal/cancel?paymentId2=PAY-90H30361WJ130342DLLMPKKQ'

    def define_payment(self):
        payment = models.Payment()
        payment.id = 'PAY-90H30361WJ130342DLLMPKKQ'
        payment.approval_url = ''
        payment.state = models.Payment.STATE_PENDING
        payment.save()
        return payment

    def test_paypal_payment_return(self):
        '''Check if database is updated after successful return request'''

        payment = self.define_payment()

        with PaypalMockContext() as ctx:
            self.get(self.return_url, 200)

        payment = models.Payment.objects.get(id=payment.id)
        self.assertEqual(payment.state, models.Payment.STATE_FINISHED)

    def test_paypal_payment_bad_return(self):
        '''Check if database is not updated after incorrect return request'''

        payment = self.define_payment()

        with PaypalMockContext() as ctx:
            self.get(self.bad_return_url, 200)

        payment = models.Payment.objects.get(id=payment.id)
        self.assertEqual(payment.state, models.Payment.STATE_PENDING)

    def test_paypal_payment_cancel(self):
        '''Check if database is not updated after successful cancel request'''

        payment = self.define_payment()

        with PaypalMockContext() as ctx:
            self.get(self.cancel_url, 200)

        payment = models.Payment.objects.get(id=payment.id)
        self.assertEqual(payment.state, models.Payment.STATE_PENDING)

    def test_paypal_payment_bad_cancel(self):
        '''Check if database is not updated after incorrect cancel request'''

        payment = self.define_payment()

        with PaypalMockContext() as ctx:
            self.get(self.bad_cancel_url, 200)

        payment = models.Payment.objects.get(id=payment.id)
        self.assertEqual(payment.state, models.Payment.STATE_PENDING)

