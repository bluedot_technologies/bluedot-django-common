from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^paypal/return$', views.paypal_return, name='payments_paypal_return'),
    url(r'^paypal/cancel$', views.paypal_cancel, name='payments_paypal_cancel'),
]

