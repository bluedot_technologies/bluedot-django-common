import paypalrestsdk

MODE_SANDBOX = 'sandbox'
MODE_LIVE = 'live'

URI_BASE = ''

def configure(uri_base, mode=MODE_SANDBOX, **kwargs):
    global URI_BASE
    URI_BASE = uri_base
    if mode == MODE_LIVE:
        live(**kwargs)
    else:
        sandbox(**kwargs)

def sandbox(paypal_client_id=None, paypal_client_secret=None):
    '''Configure all payment interfaces in`sandbox` mode'''

    paypalrestsdk.configure({
            'mode': 'sandbox',
            'client_id': paypal_client_id,
            'client_secret': paypal_client_secret,
        })

def live(paypal_client_id=None, paypal_client_secret=None):
    '''Configure all payment interfaces in `live` mode'''

    paypalrestsdk.configure({
            'mode': 'live',
            'client_id': paypal_client_id,
            'client_secret': paypal_client_secret,
        })

