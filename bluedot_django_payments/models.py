from django.db import models

class Payment(models.Model):
    '''Stores information about payment.'''

    STATE_NEW = 'N'
    STATE_PENDING = 'P'
    STATE_CANCELED = 'C'
    STATE_FINISHED = 'F'
    STATE_CHOICES = {
            (STATE_NEW,      'New'),
            (STATE_PENDING,  'Pending'),
            (STATE_CANCELED, 'Canceled'),
            (STATE_FINISHED, 'Finished'),
        }

    id = models.CharField(max_length=128, primary_key=True)
    state = models.CharField(max_length=1, choices=STATE_CHOICES, default=STATE_NEW)
    approval_url = models.URLField(null=True, default=None)

