from bluedot_testcases import mock_context

class PaymentMockContext(mock_context.MockContext):
    '''Mock context for `payments_paypal.Payment`.'''

    class PaymentMock:
        '''Mock for `payments_paypal.Payment`.'''

        def __init__(self, context):
            self.context = context

        @classmethod
        def construct(cls, context, *args):
            self.context = context
            return super(PaymentMock, cls).construct(*args)

        @classmethod
        def create(self):
            return True

        def execute(self):
            return True

        def get_id(self):
            return self.context._id


    def __init__(self):
        super(PaymentMockContext, self).__init__(
            self.PaymentMock(self),
            'bluedot_django_marketplace',
            'actions',
            'payment_constructor',
        )

        self._id = None
        self._links = []

    def fake(self, *args):
        return self.mock

    def set_id(self, id):
        self._id = id

