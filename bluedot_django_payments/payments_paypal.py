import paypalrestsdk
from paypalrestsdk import ResourceNotFound

from django.urls import reverse

from . import config, models


# Payment object construction extracted to function to make it mockable in unit tests.
def payment_class(*args):
    return paypalrestsdk.Payment


class Payment:
    '''PayPal payment'''

    def __init__(self, payment):
        self.entry = None
        self.payment = payment

    @classmethod
    def construct(cls, *args):
        return cls(payment_class()(cls._make_params(*args)))

    @classmethod
    def find(cls, payment_id):
        '''Fetches payment info from PayPal service.'''

        try:
            return cls(payment_class().find(payment_id))

        except ResourceNotFound as error:
            # TODO: Log payment search error
            return None

    @classmethod
    def _make_params(cls, amount, currency, email, item, description):
        return {
            'intent': 'sale',
            'payer': {
                'payment_method': 'paypal',
            },
            'redirect_urls': {
                'return_url': config.URI_BASE + reverse('payments_paypal_return'),
                'cancel_url': config.URI_BASE + reverse('payments_paypal_cancel'),
            },
            'transactions': [{
                'item_list': {
                    'items': [{
                        'name': item,
                        'sku': item,
                        'price': amount,
                        'currency': currency,
                        'quantity': 1,
                    }]
                },
                'payee': {
                    'email': email,
                },
                'amount': {
                    'total': amount,
                    'currency': currency,
                },
                'description': description,
            }]
        }

    def _get_entry(self):
        if not self.entry:
            self.entry = models.Payment.objects.get(id=self.payment.id)
        return self.entry

    def create(self):
        '''Registers new payment in the payment system and creates according database entry.'''

        result = self.payment.create()
        if result:
            approval_url = None
            for link in self.payment.links:
                if link.rel == 'approval_url':
                    approval_url = link.href

            entry = models.Payment()
            entry.id = self.payment.id
            entry.approval_url = approval_url
            entry.state = entry.STATE_PENDING
            entry.save()
            self.entry = entry

        else:
            # TODO: Log payment creation error
            pass

        return result

    def execute(self, payer_id):
        '''Executes the payment accepted by user in PayPal service.'''

        result = self.payment.execute({"payer_id": payer_id})
        if result:
            entry = self._get_entry()
            entry.state = entry.STATE_FINISHED
            entry.save()
        else:
            # TODO: Log payment execution error
            pass

        return result

    def cancel(cls, payment_id):
        '''Cancels the payment. For PayPal there is nothing to do.'''

        # Nothing to do

    def get_id(self):
        '''Returns payment ID.'''

        return self.payment.id

