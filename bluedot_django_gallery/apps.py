from django.apps import AppConfig


class BluedotDjangoGalleryConfig(AppConfig):
    name = 'bluedot_django_gallery'
