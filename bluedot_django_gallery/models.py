import uuid

from django.db import models

class Item(models.Model):
    '''Gallery item.'''

    gallery = models.UUIDField(default=uuid.uuid4, editable=False)
    url = models.URLField()
