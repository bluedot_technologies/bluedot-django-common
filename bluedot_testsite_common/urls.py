from django.conf.urls import include, url

urlpatterns = [
    url(r'^payments/', include('bluedot_django_payments.urls')),
]
